# Implementing a recurring rollback practice

In Q2 FY22 we implemented tooling and process to support rollbacks on Staging and Production. Throughout Q3 we're continuing to improve reliability of [Assisted Rollbacks](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/282) as a path towards [Hands-off Deployments](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/280).

Running regular practice rollbacks on Staging will help to build confidence in the tools and process. Suggested Staging rollback dates have been added to the release issue, Release Managers should attempt to rollback on these dates, or schedule for a suitable date if needed.

## Process

1. On the suggested date Release Managers should plan a Staging rollback, or schedule for a suitable date if needed.
1. A suitable package should be identified using the [runbook].
1. Rollback timing is decided by the Release Manager. Rolling back a suitable
   package once it has started to deploy to Production can help minimise the
   impact a test rollback will have on deployments and MTTP.
1. Follow the [runbook] for steps to complete the rollback.
   * Check the accuracy of the rollback pipeline. It **should** include:
      * Preparation and tracking
      * Web fleet jobs
      * QA tests
      * Manual Gitaly and Praefect jobs
   * It **should not** include:
      * Post-deploy migration jobs
1. Please open issues in the [Delivery issue tracker][issues] for problems or improvements following the test.
1. Roll forward to the package deployed prior to the rollback (essentially
   "undoing" the rollback) to [keep consistency with Production][consistency].

[runbook]: runbooks/rollback-a-deployment.md
[consistency]: runbooks/rollback-a-deployment.md#6-ensure-consistency-between-staging-and-production
[issues]: https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues
